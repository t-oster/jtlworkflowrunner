echo "Executing docker build, which will build the executable from the current folder"
docker build -t jtlworkflowrunner .
id=$(docker create jtlworkflowrunner)
docker cp $id:/usr/src/app/build/WorkflowRunner.exe WorkflowRunner.exe
docker rm $id
docker rmi jtlworkflowrunner
