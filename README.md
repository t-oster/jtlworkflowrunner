Dieses Projekt baut ein interaktives Kommandozeilenprogramm um die JtlWawiExtern.dll (https://developer.jtl-software.de/Wiki/JTLwawiExterndll_einbinden/)
Es wird mit MonoDevelop kompiliert (geht auf Linux).
Am Ende wird die .exe Datei von mir von anderen Programmen aus gestartet und per STDIN mit Befehlen versorgt, während die Ergebnisse über STDOUT zurück fliessen.


Um es auf Linux zu Compilen:

    --Installation
        sudo apt install apt-transport-https dirmngr gnupg ca-certificates
        sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
        echo "deb https://download.mono-project.com/repo/debian stable-buster main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
        sudo apt update

        sudo apt install mono-devel
    --Compilen
        In dem Hautpordner "xbuild" eingeben.
        Darauf hin wird geBuildet und Compiliert es erscheint eine EXE in der "bin" und im "obj/x86"
        

