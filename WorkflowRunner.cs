using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using JTLwawiExtern;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Linq;

namespace WorkflowRunner
{
	class WorkflowRunner
	{
		static CJTLwawiExtern wawi;
		static String pass = "sa04jT14";
		static String ipport = "192.168.1.3,1433";
		static String version = "1.3.21";
		static String databaseName = "eazybusiness";

        /*
         * Splittet nach leerzeichen unter Berücksichtigung von Anführungszeichen
         */
        private static string[] mySplit(string input, string delimiter)
        {
            return input.Split('"')
                     .Select((element, index) => index % 2 == 0  // If even index
                                           ? element.Split(new[] { delimiter }, StringSplitOptions.RemoveEmptyEntries)  // Split the item
                                           : new string[] { element })  // Keep the entire item
                     .SelectMany(element => element).ToArray<string>();
        }

            public static void RunProgram(string[] args) {
			wawi = new CJTLwawiExtern();
			try { 
				for (Int32 i=0;i<args.Length;i++)
				{
					if (args[i] == "/pass")
					{
						pass = args[++i];
					}
					else if (args[i] == "/ipport")
					{
						ipport = args[++i];
					}
					else if (args[i] == "/version")
					{
						Console.WriteLine(version);
						return;
					}
					else if(args[i] == "/databaseName"){
						databaseName = args[++i];
					}
					else
					{
						displayHelp();
						return;
					}
				}
			}
			catch
			{
				displayHelp();
				return;
			}
			Console.WriteLine ("Directmode Ready");
			try {
				while (true) {
					String line = Reader.ReadLine(180*60*1000);
					if (line == null) {
						break;
					}
					String[] parts = mySplit(line, " ");
					if (parts [0] == "exit") {
						Console.WriteLine ("bye");
						return;
					} else if (parts [0] == "JTL_WorkflowAuftrag" && parts.Length == 4) {
						Int32 kBenutzer;
						Int32 kBestellung;
						Int32 eventId;
						try {
							kBenutzer = Int32.Parse(parts[1]);
							kBestellung = Int32.Parse(parts[2]);
							eventId = Int32.Parse(parts[3]);
						}
						catch {
							System.Console.WriteLine("invalid arguments");
							continue;
						}
						System.Console.WriteLine("start");
						wawi.JTL_WorkflowAuftrag(ipport, databaseName, "sa", pass, kBenutzer, kBestellung, eventId);
						System.Console.WriteLine("done");
					} else if (parts [0] == "JTL_Korrekturbuchung" && parts.Length == 9) {
						int kBenutzer;
						int kArtikel;
						int kWarenLagerPlatz;
						double dAnzahl;
						string cKommentar;
						int kPlattForm;
						string cMHD;
						string cCharge;
						int kWarenLagerEingang;//out
						try {
							kBenutzer = int.Parse(parts[1]);
							kArtikel = int.Parse(parts[2]);
							kWarenLagerPlatz = int.Parse(parts[3]);
							dAnzahl = double.Parse(parts[4], System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.AllowLeadingSign, System.Globalization.NumberFormatInfo.InvariantInfo);
							cKommentar = parts[5];
							kPlattForm = int.Parse(parts[6]);
							cMHD = parts[7];
							cCharge = parts[8];
							if (cMHD == "null" || cMHD == null) {
								cMHD = "";
							}
							if (cCharge == "null" || cCharge == null) {
								cCharge = "";
							}
						}
						catch {
							System.Console.WriteLine("invalid arguments");
							continue;
						}
						System.Console.WriteLine("start");

						wawi.JTL_Korrekturbuchung(ipport, databaseName, "sa", pass, 
						                          kBenutzer, kArtikel, kWarenLagerPlatz, dAnzahl, cKommentar, kPlattForm, cMHD, cCharge, out kWarenLagerEingang);
						System.Console.WriteLine("done");
                    } else if (parts[0] == "VersanddatenImport" && parts.Length == 6) {
                        int kBenutzer;
                        string[] Id;
                        DateTime[] Versanddatum;
                        string[] TrackingId;
                        string[] VersandInfo;
                        try
                        {
                            kBenutzer = int.Parse(parts[1]);
                            Id = mySplit(parts[2], ",");
                            string[] tmp = mySplit(parts[3], ",");
                            Versanddatum = new DateTime[tmp.Length];
                            for (int i = 0; i < tmp.Length; i++)
                            {
                                Versanddatum[i] = DateTime.Parse(tmp[i]);
                            }
                            TrackingId = mySplit(parts[4],  ",");
                            VersandInfo = mySplit(parts[5],  ",");
                        }
                        catch (Exception e)
                        {
                            System.Console.WriteLine("invalid arguments");
                            System.Console.WriteLine("{0} First exception caught.", e);
                            continue;
                        }
                        System.Console.WriteLine("start");
                        JTLwawiExtern.VersanddatenImport.VersanddatenImporter importer = wawi.VersanddatenImporter(ipport, databaseName, "sa", pass, kBenutzer);
                        for (int i = 0; i < Id.Length; i++)
                        {
                            importer.Add(Id[i], Versanddatum[i], TrackingId[i], VersandInfo[i]);
                        }
                        importer.Apply();
                        System.Console.WriteLine("done");
                    } else {
						Console.WriteLine ("unknown command. Try:");
						Console.WriteLine ("    JTL_WorkflowAuftrag kBenutzer kBestellung eventId");
						Console.WriteLine ("    JTL_Korrekturbuchung kBenutzer kArtikel kWarenLagerPlatz dAnzahl cKommentar kPlattForm cMHD cCharge");
                        Console.WriteLine("   VersanddatenImport kBenutzer Id Versanddatum TrackingId VersandInfo");
                        break;
					}
				}
			}
			catch (Exception e) {
				Console.WriteLine("An error occurred: '{0}'", e);
				Process.GetCurrentProcess().Kill();
			}
		}

		private static void displayHelp()
		{
			Console.WriteLine("WorkflowRunner [/pass <pass>] [/ipport <ip,port>] [/databaseName <databasename>]");
			Console.WriteLine("WorkflowRunner /version");
		}

	}
}
