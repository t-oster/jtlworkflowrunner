﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JTLwawiExtern;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace WorkflowRunner
{
    class Program
    {

		public static void exception_handler(object sender, UnhandledExceptionEventArgs e) {
			try
			{
				Exception ex = (Exception)e.ExceptionObject;
				Console.WriteLine("Unhandeled Exception" + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace);
			}
			catch
			{
				Console.WriteLine ("Exception beim LOG schreiben. Wird beendet...");
			}
			finally {
				Process.GetCurrentProcess().Kill();
			}
		}

		[STAThread]
        static void Main(string[] args) {
			AppDomain.CurrentDomain.UnhandledException +=
				new UnhandledExceptionEventHandler (exception_handler);
			/*
             * - Wawi Installationspfad finden -> Registry 
             * - JTLwawiExtern finden und Version prüfen
             * - ALLE JTL Assemblys aus dem Installationspfad laden
             */
			ValidateAssembly validateAssembly = new ValidateAssembly ();
			if (!validateAssembly.IsValid) {
				Console.WriteLine ("Fehler bei Wawi DLL Suche...");
				return;
			}
			WorkflowRunner.RunProgram (args);
		}
	}
}
